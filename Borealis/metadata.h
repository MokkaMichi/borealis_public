#pragma once

#define STARTUP_JOBS		17

#define STR(x)				#x

#define STATUS				indev
#define QSTATUS				STR(indev)


#define MAIN_VERSION		0
#define MINOR_VERSION		1
#define BUILD_NUMBER		001