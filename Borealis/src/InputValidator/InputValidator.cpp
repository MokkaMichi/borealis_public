#include "InputValidator.h"

bool InputValidator::validate(std::string input, InputTypes inputtype)
{

	unsigned char length;

	switch (inputtype) {
	case INPUTTYPE_STEAMID:
		length = 17;
		break;
	case INPUTTYPE_GUID:
		length = 32;
		break;
	default:
		length = 0;
	}

	if (input.length() != length)
		return false;

	return true;
}
