#pragma once

#include "../../pch.h"

class InputValidator
{
public:
	enum InputTypes {
		INPUTTYPE_STEAMID,
		INPUTTYPE_GUID
	};
private:
	InputValidator() {}
public:
	static bool validate(std::string input, InputTypes inputtype);

};

