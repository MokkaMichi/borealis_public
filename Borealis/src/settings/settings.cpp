#include "settings.h"

Settings::Settings(const std::string & filepath) :
	m_Filepath(filepath)
{

	
	load();
}

void Settings::load()
{
	m_INIReader = new INIReader(m_Filepath);

	int n = 3;
	ProgressBar *bar = new ProgressBar(n, "Setting up paths from INI...");
	bar->Progressed(0);
	std::this_thread::sleep_for(std::chrono::milliseconds(200));

	m_DatabasePath = m_INIReader->Get("Path Settings", "DatabasePath", "");
	bar->Progressed(1);
	std::this_thread::sleep_for(std::chrono::milliseconds(150));

	m_InfiStarPath = m_INIReader->Get("Path Settings", "InfiStarPath", "");
	bar->Progressed(2);
	std::this_thread::sleep_for(std::chrono::milliseconds(250));

	m_BECAdminsPath = m_INIReader->Get("Path Settings", "BECAdminsPath", "");
	bar->Progressed(3);
	std::this_thread::sleep_for(std::chrono::milliseconds(100));

	std::cout << std::endl;
}
