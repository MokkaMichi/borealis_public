#pragma once

#include "../../pch.h"

class Settings {
private:
	std::string m_Filepath;

	INIReader* m_INIReader;
	
	std::string m_DatabasePath;
	std::string m_InfiStarPath;
	std::string m_BECAdminsPath;
public:
	Settings(const std::string &filepath);

	inline std::string getDataBasePath() const { return m_DatabasePath; }
	inline std::string getInfiStarPath() const { return m_InfiStarPath; }
	inline std::string getBECAdminsPath() const { return m_BECAdminsPath; }

private:
	void load();
};