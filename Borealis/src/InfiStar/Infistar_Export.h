#pragma once

#include "../../pch.h"
#include "../database/database.h"



class InfiStar_Export {

private:
	InfiStar_Export() { };
public:
	static bool exportData(std::string filepath, Database* database);
};