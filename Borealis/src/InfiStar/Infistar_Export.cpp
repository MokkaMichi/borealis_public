#include "Infistar_Export.h"


bool InfiStar_Export::exportData(std::string filepath, Database* database)
{

	int n = 150;

	ProgressBar *bar = new ProgressBar(n, "Exporting Database for InfiStar...");

	for (int i = 0; i <= n; ++i) {
		bar->Progressed(i);
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	std::cout << std::endl;

	std::string steamID_temp;
	std::string name_temp;
	std::string rank_temp;

	std::ofstream adminFile;
	adminFile.open(filepath, std::ios::out | std::ios::trunc);
	adminFile << 
		"/*" << std::endl <<
		"*		infiStar_A3: Admin File" << std::endl <<
		"*" << std::endl <<
		"*		Exported from database by Borealis" << std::endl <<
		"*/" << std::endl << std::endl << std::endl;

	for (int i = 0; i < NUMBER_OF_ADMINLEVELS; i++) {
		
		std::vector<Member> adminBuffer = database->getMembersByAdminLevel(Member::toAdminLevel(std::to_string(i + 1)));

		if (adminBuffer.size() > 0)
			adminFile << "_adminLevel_" << (i + 1) << " = [" << std::endl;
		else
			adminFile << "_adminLevel_" << (i + 1) << " = [];" << std::endl << std::endl;


		for (unsigned int j = 0; j < adminBuffer.size(); j++) {

			name_temp = adminBuffer.at(j).getName();
			steamID_temp = adminBuffer.at(j).getSteamID();
			rank_temp = adminBuffer.at(j).getRankByName(true);

			adminFile << "	'" << steamID_temp << "'";

			if (j == adminBuffer.size() - 1) {

				adminFile << "			/* " << name_temp << " - " << rank_temp << " */" << std::endl << "];" << std::endl << std::endl;
			}
			else {
				adminFile << ",		/* " << name_temp << " - " << rank_temp << " */" << std::endl;
			};
		}

	}

	adminFile.close();
	
	return true;
}
