﻿#pragma once

#include "../../pch.h"

struct FileUtils {
	static std::string read_file(const std::string& filepath) {

		std::ifstream ifs((filepath).c_str());
		std::string content(
			std::istreambuf_iterator<char>(ifs.rdbuf()),
			std::istreambuf_iterator<char>()
		);
		return content;
	}

	static bool exists_file(const std::string& filepath) {

		struct stat buffer;
		return (stat(filepath.c_str(), &buffer) == 0);
	}

	static std::string backup_file(const std::string& filepath) {
		int n = 100;

		std::string title = "Backing Up File " + filepath + "...";

		ProgressBar *bar = new ProgressBar(n, title.c_str());

		for (int i = 0; i <= n; ++i) {
			bar->Progressed(i);
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
		std::cout << std::endl;

		remove((filepath + ".bak").c_str());
		std::ofstream ofs(filepath + ".bak");

		ofs << read_file(filepath);

		return (filepath + ".bak");
	}


	static bool encrypt_file(const std::string& filepath) {

		ProgressBar *bar2 = new ProgressBar(6, "Encrypting Database File...");

		bar2->Progressed(0);
		std::this_thread::sleep_for(std::chrono::milliseconds(150));

		std::fstream source,temp;
		std::ofstream tempOut("~temp");

		bar2->Progressed(1);
		std::this_thread::sleep_for(std::chrono::milliseconds(100));

		source.open(filepath, std::ios::in);

		source >> std::noskipws;
		char c;

		bar2->Progressed(2);
		std::this_thread::sleep_for(std::chrono::milliseconds(200));

		while (source >> c) {
			c = c + 100;
			tempOut << c;
		}
		
		bar2->Progressed(3);
		std::this_thread::sleep_for(std::chrono::milliseconds(250));

		source.close();
		tempOut.close();

		source.open(filepath, std::ios::out | std::ios::trunc);
		temp.open("~temp", std::ios::in);

		bar2->Progressed(4);
		std::this_thread::sleep_for(std::chrono::milliseconds(50));

		while (temp >> c) {
			source << c;

		}

		bar2->Progressed(5);
		std::this_thread::sleep_for(std::chrono::milliseconds(300));

		source.close();
		temp.close();

		remove("~temp");
		
		bar2->Progressed(6);
		std::this_thread::sleep_for(std::chrono::milliseconds(75));
		std::cout << std::endl;

		return true;
	}

	static bool decrypt_file(const std::string& filepath) {

		ProgressBar *bar3 = new ProgressBar(5, "Decrypting Database File...");

		bar3->Progressed(0);
		std::this_thread::sleep_for(std::chrono::milliseconds(150));

		std::fstream source, temp;
		std::ofstream tempOut("~temp");

		source.open(filepath, std::ios::in);
		source >> std::noskipws;
		char c;
		
		bar3->Progressed(1);
		std::this_thread::sleep_for(std::chrono::milliseconds(200));

		while (source >> c) {
			tempOut << c;
		}

		bar3->Progressed(2);
		std::this_thread::sleep_for(std::chrono::milliseconds(150));		

		source.close();
		tempOut.close();

		source.open(filepath, std::ios::out | std::ios::trunc);
		temp.open("~temp", std::ios::in);
		
		bar3->Progressed(3);
		std::this_thread::sleep_for(std::chrono::milliseconds(300));

		while (temp >> c) {
			c = c - 100;
			source << c;
		}

		bar3->Progressed(4);
		std::this_thread::sleep_for(std::chrono::milliseconds(50));

		source.close();
		temp.close();

		remove("~temp");
		
		bar3->Progressed(5);
		std::this_thread::sleep_for(std::chrono::milliseconds(350));

		std::cout << std::endl;

		return true;
	}
};