#pragma once

#include <string>

#include "../../pch.h"
#include "../database/database.h"
#include "../member/member.h"
#include "../BEC/BEC_Export.h"
#include "../InfiStar/Infistar_Export.h"

#include "../settings/settings.h"
#include "../util/fileutils.h"
#include "../util/beguid.h"
#include "../util/progressbars/progress_bar.h"


class ConsoleOutput {
public:
	enum SELECTIONS {
		A, B, C, D, E, F, G, H, J, K, L, M, N, O, X, Y, Z, NIL = -1
	};
private:

public:
	Database* m_Database;
	Settings* m_Settings;

	ConsoleOutput(Database* database) : m_Database(database) { }
	ConsoleOutput() { }
	void init();

	void mainMenu();
	void selectionMenu();

	void setDatabase(Database* database);

	static void printBanner();
	static void printVersion();

	static void clearScreen();
	static void println(std::string msg);
	inline static void printNewLine(const char& number = 1) { for (char i = 0; i < number; i++) std::cout << std::endl; }

private:
	static SELECTIONS getSelection(char sel);

	void handleSelection(SELECTIONS sel);

	static void printAddMember(short step);

	void getMemberName(std::string& nameRef);
	void getMemberSteamID(std::string& steamIDRef);
	void getMemberAdminLevel(Member::AdminLevels& adminLevelRef);
	void getMemberRank(Member::Ranks& rankRef);
	void getMemberGroupName(Member::GroupNames& groupNameRef);

	void printAbout();
	void printLicense();
	void printHelp();

	void repairDatabase(const std::string& db_path);

	void printDatabase();
	void printSmallDatabase();
	void addMember();
	void editMember();
	void deleteMember();
};