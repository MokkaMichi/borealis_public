#include "console_out.h"

void ConsoleOutput::init()
{
	system("mode 650");
	ShowWindow(GetConsoleWindow(), SW_MAXIMIZE);
	SendMessage(GetConsoleWindow(), WM_SYSKEYDOWN, VK_RETURN, 0x20000000);

	printBanner();
	printVersion();

	println("Starting Up...");
	println("--------------");
	printNewLine(3);


	
	m_Settings = new Settings ("borealis.ini");
	m_Database = new Database(m_Settings->getDataBasePath());

	FileUtils::backup_file(m_Settings->getDataBasePath());
	FileUtils::decrypt_file(m_Settings->getDataBasePath());
	m_Database->load();
	FileUtils::encrypt_file(m_Settings->getDataBasePath());


	printNewLine(2);
	println("Borealis is ready for use!");
	printNewLine(5);
	system("PAUSE");
	clearScreen();

	mainMenu();
	system("PAUSE");
}

void ConsoleOutput::printBanner()
{
	std::cout << R"(
 ____                        __            
|  _ \                       \ \           
| |_) ) ___   ___  ___ __  __ \ \  _  ____ 
|  _ ( / _ \ / _ \/ __)  \/ /  > \| |/  ._)
| |_) | (_) ) |_) > _| ()  <  / ^ \ ( () ) 
|____/ \___/|  __/\___)__/\_\/_/ \_\_)__/  
            | |                            
            |_|   
)";
	printNewLine(4);
}

void ConsoleOutput::printVersion()
{
	std::cout << 
		"Programm Version: " << QSTATUS << "_" << MAIN_VERSION << "_" << MINOR_VERSION << "_build" << BUILD_NUMBER << std::endl << std::endl << 
		"C++ Version: " << __cplusplus;
	printNewLine(3);
}

void ConsoleOutput::mainMenu()
{
	println("Borealis Main Menu");
	println("------------------");
	printNewLine(3);

	println("Welcome to the Borealis Admin Management tool for Arma 3. This program currently supports managing a synchronized admin system using BEC and InfiStar_A3.");
	println("Borealis uses a common database to populate both admin systems independently.");
	printNewLine(2);
	println("This is the main menu. Please select one of the below options by entering the preceeding letter.");
	println("E.g: 'Z' for Help.");
	printNewLine(3);
	selectionMenu();
}

void ConsoleOutput::selectionMenu()
{
	println("A. View entire Database");
	println("B. Add a member");
	println("C. Edit a member");
	println("D. Remove a member");
	printNewLine();
	println("E. Export Database to BEC");
	println("F. Export Database to InfiStar");
	printNewLine();
	println("G. Save the Database");
	println("H. Load the Database");
	println("J. Clear the Database");
	printNewLine();
	println("K. Save, Export and Exit");
	println("L. Save and Exit");
	println("M. Exit without Saving");
	printNewLine();
	println("N. Repair Database (Not Implemented)");
	println("O. Rebuild Database");
	printNewLine();
	println("X. About (Not Implemented)");
	println("Y. License (Not Implemented)");
	println("Z. Help (Not Implemented)");
	printNewLine(3);

	std::cout << "> ";
	char response = _getch();
	printNewLine(2);

	
	handleSelection(getSelection(response));
}

void ConsoleOutput::setDatabase(Database * database)
{
	m_Database = database;
}

void ConsoleOutput::handleSelection(SELECTIONS sel)
{
	char response;

	int n = 250;

	ProgressBar *bar;
	
	switch (sel) {

	case A:
		printDatabase();
		break;
	case B:
		addMember();
		break;
	case C:
		editMember();
		break;
	case D:
		deleteMember();
		break;
	case E:

		FileUtils::backup_file(m_Settings->getBECAdminsPath());
		BEC_Export::exportData(m_Settings->getBECAdminsPath(), m_Database);
		break;
	case F:
		FileUtils::backup_file(m_Settings->getInfiStarPath());
		InfiStar_Export::exportData(m_Settings->getInfiStarPath(), m_Database);
		break;
	case G:
		FileUtils::decrypt_file(m_Settings->getDataBasePath());
		m_Database->save();
		FileUtils::encrypt_file(m_Settings->getDataBasePath());
		break;
	case H:
		FileUtils::decrypt_file(m_Settings->getDataBasePath());
		m_Database->load();
		FileUtils::encrypt_file(m_Settings->getDataBasePath());
		break;
	case J:
		println("Are you sure that you want to clear the database?");
		println("This will delete all data. There is no undo!");
		printNewLine(3);
		println("Press Y to clear the database.");
		std::cout << "> ";
		response = _getch();

		switch (response) {
		case ('Y'):
		case ('y'):
			bar = new ProgressBar(n, "Clearing Database...");

			for (int i = 0; i <= n; ++i) {
				bar->Progressed(i);
				std::this_thread::sleep_for(std::chrono::milliseconds(10));
			}
			std::cout << std::endl;
			m_Database->clear();
			break;
		default:
			break;
		}
		break;
	case K:		
		FileUtils::backup_file(m_Settings->getBECAdminsPath());
		BEC_Export::exportData(m_Settings->getBECAdminsPath(), m_Database);

		FileUtils::backup_file(m_Settings->getInfiStarPath());
		InfiStar_Export::exportData(m_Settings->getInfiStarPath(), m_Database);
		FileUtils::decrypt_file(m_Settings->getDataBasePath());
		m_Database->save();
		FileUtils::encrypt_file(m_Settings->getDataBasePath());
		system("PAUSE");
		exit(EXIT_SUCCESS);
	case L:
		FileUtils::decrypt_file(m_Settings->getDataBasePath());
		m_Database->save();
		FileUtils::encrypt_file(m_Settings->getDataBasePath());
		system("PAUSE");
		exit(EXIT_SUCCESS);
	case M:
		system("PAUSE");
		exit(EXIT_SUCCESS);
	case N:
		repairDatabase(m_Settings->getDataBasePath());
		break;
	case O:
		bar = new ProgressBar(n, "Rebuilding Database...");
		m_Database->reorder();

		for (int i = 0; i <= 100; ++i) {
			bar->Progressed(i);
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
		std::cout << std::endl;
		break;
	case X:
		printAbout();
		break;
	case Y:
		printLicense();
		break;
	case Z:
		printHelp();
		break;
	}
	clearScreen();
	mainMenu();
}

void ConsoleOutput::clearScreen()
{
	system("cls");
	printBanner();
	printVersion();
}

void ConsoleOutput::printDatabase()
{
	clearScreen();
	std::cout << *m_Database;
	
	printNewLine(3);
	system("PAUSE");
	clearScreen();
	mainMenu();
}

void ConsoleOutput::printSmallDatabase()
{
	println("Database Synopsis: ");
	printNewLine(3);
	println("Index:	Name:");
	for (int i = 0; i < m_Database->getMemberCount(); i++) {

		std::cout <<	m_Database->getMemberAtIndex(i).getID() << "	" <<
						m_Database->getMemberAtIndex(i).getName() << std::endl;
	}
	printNewLine(4);
}

void ConsoleOutput::addMember()
{
	printAddMember(0);
	println("Please follow on-screen instructions to add a new member to the Database.");
	println("");
	println("Press X to return to the selection, or any other key to continue...");
	std::cout << "> ";
	char response = _getch();

	switch (response) {
	case ('x'):
	case ('X'):
		clearScreen();
		mainMenu();
		break;
	default:
		printAddMember(1);
	}

	//------------------------- Add a Member, actual routine
	std::string memberName, memberSteamID;
	Member::AdminLevels memberAdminLevel;
	Member::Ranks memberRank;

	bool valuesCorrect = false;

	//------------------------- Name and SteamID

	while (!valuesCorrect) {

		println("Member Name and SteamID");
		printNewLine(3);

		getMemberName(memberName);

		printNewLine(2);

		getMemberSteamID(memberSteamID);

		printNewLine(5);

		println("Please check your input and press any key to continue.");
		println("Pressing the X key will let you return to the selection screen.");
		printNewLine();
		std::cout << "Name: " << memberName << std::endl;
		std::cout << "SteamID: " << memberSteamID << std::endl;
		printNewLine();
		std::cout << "> ";

		response = _getch();

		switch (response) {
		case ('x'):
		case ('X'):
			clearScreen();
			mainMenu();
			break;
		default:
			printAddMember(2);
			valuesCorrect = true;
		}
	}

	//------------------------- AdminLevel

	valuesCorrect = false;
	while (!valuesCorrect) {

		println("Member Admin-Level");
		printNewLine(3);

		getMemberAdminLevel(memberAdminLevel);

		printNewLine(5);

		println("Please check your input and press any key to continue.");
		println("Pressing the X key will let you return to the selection screen.");
		printNewLine();
		std::cout << "Selected Adminlevel: " << memberAdminLevel << std::endl;
		printNewLine();
		std::cout << "> ";

		response = _getch();

		switch (response) {
		case ('x'):
		case ('X'):
			clearScreen();
			mainMenu();
			break;
		default:
			printAddMember(3);
			valuesCorrect = true;
		}
	}

	//------------------------- Rank

	valuesCorrect = false;
	while (!valuesCorrect) {

		println("Member Rank");
		printNewLine(3);

		getMemberRank(memberRank);

		printNewLine(5);

		println("Please check your input and press any key to continue.");
		println("Pressing the X key will let you return to the selection screen.");
		printNewLine();
		std::cout << "Rank: " << Member::getRankByEnum(memberRank) << std::endl;
		printNewLine(); 
		std::cout << "> ";

		response = _getch();

		switch (response) {
		case ('x'):
		case ('X'):
			clearScreen();
			mainMenu();
			break;
		default:
			printAddMember(4);
			valuesCorrect = true;
		}
	}
	
	//------------------------- Last check

	println("Please have a quick look at the data, before submitting it to the Database: ");
	printNewLine(3);

	Member newMember(0, memberName, memberSteamID, memberAdminLevel, memberRank);

	std::cout << newMember;

	printNewLine(3);

	println("Press any key to continue, press X to return to the selection screen without submitting the new member to the Database.");

	response = _getch();

	switch (response) {
	case 'X':
	case 'x':
		clearScreen();
		mainMenu();
		break;
	default:
		clearScreen();
		println		("	Add a new Member to the Database");
		println		("	--------------------------------");
		std::cout << "	         Finalizing...          ";
		printNewLine(4);
		break;
	}

	newMember.updateID(m_Database->getMemberCount() + 1);

	int kappa = 40;
	ProgressBar *bar = new ProgressBar(kappa, "Compiling Member Data...");

	for (int i = 0; i <= kappa; ++i) {
		bar->Progressed(i);
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	printNewLine(1);

	m_Database->addMember(new Member(newMember));

	kappa = 140;
	ProgressBar *bar2 = new ProgressBar(kappa, "Adding Member to Database...");

	for (int i = 0; i <= kappa; ++i) {
		bar2->Progressed(i);
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	printNewLine(1);

	m_Database->reorder();

	kappa = 200;
	ProgressBar *bar3 = new ProgressBar(kappa, "Rebuilding Database...");

	for (int i = 0; i <= kappa; ++i) {
		bar3->Progressed(i);
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	printNewLine(5);

	system("PAUSE");

	mainMenu();
	selectionMenu();


}

void ConsoleOutput::editMember()
{
	int memberID = 0;
	printSmallDatabase();
	println("Please enter the ID of the member that you want to edit.");
	println("X lets you return to the selection screen.");
	printNewLine(2);

	std::cout << "> ";
	std::string resp;
	std::getline(std::cin, resp);
	
	printNewLine(2);

	if (resp == "X" || resp == "x") {
		clearScreen();
		mainMenu();
	} else {
		memberID = std::atoi(resp.c_str());
		if (memberID > m_Database->getMemberCount()) {
			println("Supplied ID exceeds possible range!");
			printNewLine(3);
			system("PAUSE");
			clearScreen();
			mainMenu();
			return;
		}
	}

	Member workCopy = m_Database->getMemberAtIndex(memberID);

	int kappa = 200;
	ProgressBar *bar2 = new ProgressBar(kappa, "Loading Member from Database...");

	for (int i = 0; i <= kappa; ++i) {
		bar2->Progressed(i);
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	printNewLine(3);

	clearScreen();
	println("Editing Member...");
	printNewLine(3);

	std::cout << workCopy;
	printNewLine(2);

	println("Please select the component you want to change.");
	println("X lets you return to the selection screen.");
	printNewLine(3);

	println("A. Name");
	println("B. SteamID");
	println("C. Admin-Level");
	println("D. Rank");
	printNewLine();
	println("X. Return to selection screen");
	printNewLine(2);
	
	std::cout << "> ";
	char response = _getch();
	printNewLine(2);

	std::string newName;
	std::string newSteamID;
	std::string newAdminLevel;
	std::string newRank;
	char letter = 'A';
	int n;

	ProgressBar *bar;


	switch (response) {

	case 'A':
	case 'a':
		clearScreen();


		println("Edit Member Name");
		printNewLine(3);

		std::cout << "The member's current name is: " << workCopy.getName();
		printNewLine(2);
		std::cout << "Please enter the member's new name: ";
		std::getline(std::cin, newName);

		workCopy.updateName(newName);

		printNewLine(2);
		println("The member's name has been updated.");
		printNewLine(2);

		m_Database->setMemberAtIndex(memberID, workCopy);

		n = 140;

		bar = new ProgressBar(n, "Saving edited Member to Database...");

		for (int i = 0; i <= n; ++i) {
			bar->Progressed(i);
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
		printNewLine(2);


		system("PAUSE");
		break;
	case 'B':
	case 'b':
		clearScreen();

		println("Edit Member SteamID");
		printNewLine(3);

		std::cout << "The member's current SteamID is: " << workCopy.getSteamID();
		printNewLine(2);
		std::cout << "Please enter the member's new SteamID: ";
		std::getline(std::cin, newSteamID);

		workCopy.updateName(newSteamID);

		printNewLine(2);
		println("The member's SteamID has been updated.");
		printNewLine(2);

		m_Database->setMemberAtIndex(memberID, workCopy);

		n = 140;

		bar = new ProgressBar(n, "Saving edited Member to Database...");

		for (int i = 0; i <= n; ++i) {
			bar->Progressed(i);
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
		printNewLine(2);
		system("PAUSE");

		break;
	case 'C':
	case 'c':
		clearScreen();

		println("Edit Member Admin-Level");
		printNewLine(3);

		std::cout << "The member's current Admin-Level is: " << workCopy.getAdminLevelByName();
		printNewLine(2);
		std::cout << "Please enter the member's new Admin-Level: ";
		std::getline(std::cin, newAdminLevel);

		workCopy.updateAdminLevel(Member::toAdminLevel(newAdminLevel));
		workCopy.updateGroupName(workCopy.makeGroupName());

		printNewLine(2);
		println("The member's Admin-Level has been updated.");

		printNewLine(2);

		m_Database->setMemberAtIndex(memberID, workCopy);

		n = 140;

		bar = new ProgressBar(n, "Saving edited Member to Database...");

		for (int i = 0; i <= n; ++i) {
			bar->Progressed(i);
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
		printNewLine(2);

		system("PAUSE");
		break;
	case 'D':
	case 'd':
		clearScreen();

		println("Edit Member Rank");
		printNewLine(3);

		std::cout << "The member's current Rank is: " << workCopy.getRankByName(true);
		printNewLine(2);


		std::cout << "Please enter the member's new Rank: ";
		std::getline(std::cin, newRank);

		workCopy.updateRank(Member::toRank(newRank));
		workCopy.updateGroupName(workCopy.makeGroupName());

		printNewLine(2);
		println("The member's Rank has been updated.");

		printNewLine(2);

		m_Database->setMemberAtIndex(memberID, workCopy);

		n = 140;

		bar = new ProgressBar(n, "Saving edited Member to Database...");

		for (int i = 0; i <= n; ++i) {
			bar->Progressed(i);
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
		printNewLine(2);

		system("PAUSE");

		break;
	case 'X':
	case 'x':
	default:
		break;
	}

	clearScreen();
	mainMenu();
	

}

void ConsoleOutput::deleteMember()
{
	int memberID = 0;
	printSmallDatabase();
	println("Please enter the ID of the member that you want to delete.");
	println("X lets you return to the selection screen.");
	printNewLine(2);

	std::cout << "> ";
	std::string resp;
	std::getline(std::cin, resp);

	if (resp == "X" || resp == "x") {

		clearScreen();
		mainMenu();
	} else {
		memberID = std::atoi(resp.c_str());
		if (memberID > m_Database->getMemberCount()) {
			println("Supplied ID exceeds possible range!");
			printNewLine(3);
			system("PAUSE");
			clearScreen();
			mainMenu();
			return;
		}
	}

	Member workCopy = m_Database->getMemberAtIndex(memberID);

	int kappa = 200;
	ProgressBar *bar2 = new ProgressBar(kappa, "Loading Member from Database...");
	ProgressBar *bar;

	for (int i = 0; i <= kappa; ++i) {
		bar2->Progressed(i);
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	printNewLine(3);

	clearScreen();
	println("Delete Member?");
	println("Are you sure that you want to delete this entry? There is no undo!");
	printNewLine(3);
	std::cout << workCopy;
	printNewLine(2);
	println("Delete?");
	printNewLine();
	println("(Y)es");
	println("(N)o");
	printNewLine(2);
	std::cout << "> ";
	char response = _getch();

	switch (response) {
	case 'Y':
	case 'y':
		m_Database->deleteMemberAtIndex(memberID);
		bar = new ProgressBar(40, "Deleting Member...");
		for (int i = 0; i <= 40; ++i) {
			bar2->Progressed(i);
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
		break;
	case 'N':
	case 'n':
	default:
		printNewLine(3);
		system("PAUSE");
		
		clearScreen();
		mainMenu();
	}

	clearScreen();
	mainMenu();
}

void ConsoleOutput::printAddMember(short step)
{
	clearScreen();
	println("	Add a new Member to the Database");
	println("	--------------------------------");
	std::cout << R"(	            Step )" << step << R"(/4)"; // 5 for Groupname enabled
	printNewLine(4);
}

void ConsoleOutput::getMemberName(std::string & nameRef)
{
	std::cout << "Please enter the member's name: ";
	std::getline(std::cin, nameRef);
}

void ConsoleOutput::getMemberSteamID(std::string & steamIDRef)
{
	std::cout << R"(Please enter/paste the player's SteamID: )";
	std::getline(std::cin, steamIDRef);
}

void ConsoleOutput::getMemberAdminLevel(Member::AdminLevels & adminLevelRef)
{
	println("Please select the member's adminlevel by entering the corresponding letter.");
	println("Enter 'X' lets you return to the selection screen.");
	printNewLine(2);

	char letter = 'A';

	for (short i = 0; i < NUMBER_OF_ADMINLEVELS; i++) {

		std::cout << letter << ". Admin Level " << i + 1 << std::endl;
		letter++;
	}
	printNewLine(3);

	std::cout << "> ";
	char response = _getch();

	switch (response) {
	case 'A':
	case 'a':
		adminLevelRef = Member::ADMIN_LEVEL_ONE;
		break;
	case 'B':
	case 'b':
		adminLevelRef = Member::ADMIN_LEVEL_TWO;
		break;
	case 'C':
	case 'c':
		adminLevelRef = Member::ADMIN_LEVEL_THREE;
		break;
	case 'D':
	case 'd':
		adminLevelRef = Member::ADMIN_LEVEL_FOUR;
		break;
	case 'X':
	case 'x':
	default:
		clearScreen();
		selectionMenu();
		break;
	}


}

void ConsoleOutput::getMemberRank(Member::Ranks & rankRef)
{
	println("Please select the member's rank by entering the corresponding letter.");
	println("Enter 'X' lets you return to the selection screen.");
	printNewLine(2);

	char letter = 'A';

	for (short i = 0; i < NUMBER_OF_RANKS; i++) {

		std::cout << letter << ". " << Member::getRankByIndex(i) << "." << std::endl;
		letter++;
		if (letter == 'I')
			letter = 'J';
	}
	printNewLine(3);

	std::cout << "> ";
	char response = _getch();

	switch (response) {
	case 'A':
	case 'a':
		rankRef = Member::RANK_CPT;
		break;
	case 'B':
	case 'b':
		rankRef = Member::RANK_2LT;
		break;
	case 'C':
	case 'c':
		rankRef = Member::RANK_LT;
		break;
	case 'D':
	case 'd':
		rankRef = Member::RANK_CM;
		break;
	case 'E':
	case 'e':
		rankRef = Member::RANK_WO;
		break;
	case 'F':
	case 'f':
		rankRef = Member::RANK_SGT;
		break;
	case 'G':
	case 'g':
		rankRef = Member::RANK_CPL;
		break;
	case 'H':
	case 'h':
		rankRef = Member::RANK_LCPL;
		break;
	case 'J':
	case 'j':
		rankRef = Member::RANK_PTE;
		break;
	case 'K':
	case 'k':
		rankRef = Member::RANK_RCT;
		break;
	case 'L':
	case 'l':
		rankRef = Member::RANK_UNRANKED;
		break;

	case 'X':
	case 'x':
	default:
		clearScreen();
		selectionMenu();
		break;
	}

}

void ConsoleOutput::getMemberGroupName(Member::GroupNames & groupNameRef)
{
	println("Please select the member's group name by entering the corresponding letter.");
	println("Enter 'X' lets you return to the selection screen.");
	printNewLine(2);

	char letter = 'A';

	for (short i = 0; i < NUMBER_OF_GROUPS; i++) {

		std::cout << letter << ". " << Member::getGroupByIndex(i) << std::endl;
		letter++;
		if (letter == 'I')
			letter = 'J';
	}
	printNewLine(3);

	std::cout << "> ";
	char response = _getch();

	switch (response) {

	case 'A':
	case 'a':
		groupNameRef = Member::GROUP_GLOBAL_ADMIN;
		break;
	case 'B':
	case 'b':
		groupNameRef = Member::GROUP_COMMUNITY_MEMBER;
		break;
	case 'C':
	case 'c':
		groupNameRef = Member::GROUP_SENIOR_ADMIN;
		break;
	case 'D':
	case 'd':
		groupNameRef = Member::GROUP_SERVER_ADMIN;
		break;
	case 'E':
	case 'e':
		groupNameRef = Member::GROUP_JUNIOR_ADMIN;
		break;
	default:
		clearScreen();
		selectionMenu();
		break;
	}
}

void ConsoleOutput::printAbout()
{
}

void ConsoleOutput::printLicense()
{
}

void ConsoleOutput::printHelp()
{
}

void ConsoleOutput::repairDatabase(const std::string & db_path)
{
	
}

void ConsoleOutput::println(std::string msg)
{
	std::cout << msg;
	printNewLine();
}

ConsoleOutput::SELECTIONS ConsoleOutput::getSelection(char sel)
{

	if (sel == 'A' || sel == 'a')
		return SELECTIONS::A;
	else if (sel == 'B' || sel == 'b')
		return SELECTIONS::B;
	else if (sel == 'C' || sel == 'c')
		return SELECTIONS::C;
	else if (sel == 'D' || sel == 'd')
		return SELECTIONS::D;
	else if (sel == 'E' || sel == 'e')
		return SELECTIONS::E;
	else if (sel == 'F' || sel == 'f')
		return SELECTIONS::F;
	else if (sel == 'G' || sel == 'g')
		return SELECTIONS::G;
	else if (sel == 'H' || sel == 'h')
		return SELECTIONS::H;
	else if (sel == 'J' || sel == 'j')
		return SELECTIONS::J;
	else if (sel == 'K' || sel == 'k')
		return SELECTIONS::K;
	else if (sel == 'L' || sel == 'l')
		return SELECTIONS::L;
	else if (sel == 'M' || sel == 'm')
		return SELECTIONS::M;
	else if (sel == 'N' || sel == 'n')
		return SELECTIONS::N;
	else if (sel == 'O' || sel == 'o')
		return SELECTIONS::O;
	else if (sel == 'X' || sel == 'x')
		return SELECTIONS::X;
	else if (sel == 'Y' || sel == 'y')
		return SELECTIONS::Y;
	else if (sel == 'Z' || sel == 'z')
		return SELECTIONS::Z;
	else
		return SELECTIONS::NIL;
}
