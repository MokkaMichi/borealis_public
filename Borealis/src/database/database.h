#pragma once


#include "../../pch.h"
#include "../member/member.h"

#include "../util/fileutils.h"

class Database
{
private:


	std::string m_FilePath;
	bool m_FilePathSet;

	unsigned short m_MemberCount;
	std::vector<Member> m_Members;

public:
	Database(const std::string& filepath);
	Database();
	~Database();

	bool save();
	bool load();

	int addMember(Member& member);
	int addMember(Member* member);
	bool delMember(const int& id);

	void setFilePath(const std::string& filepath) { m_FilePath = filepath; }
	void update() const;
	void clear();

	std::vector<Member> getMembersByAdminLevel(Member::AdminLevels adminlvl);

	inline unsigned short getMemberCount() const { return m_MemberCount; }
	inline Member getMemberAtIndex(const unsigned short& i = 0) const { return m_Members.at(i); }
	void setMemberAtIndex(const unsigned short& i, Member member);

	void deleteMemberAtIndex(const unsigned short& i);

	void reorder();

	friend std::ostream& operator<< (std::ostream& os, const Database& database);

private:
	inline static void LOGERR(const std::string& msg) { std::cout << "[DATABASE] (ERROR) " << msg.c_str() << std::endl; }
};