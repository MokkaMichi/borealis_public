#include "database.h"

Database::Database(const std::string & filepath) :
	m_FilePath(filepath)
{
	
	m_MemberCount = 0;
	m_FilePathSet = true;

}

Database::Database()
{
	m_MemberCount = 0;
	m_FilePathSet = false;
}

Database::~Database()
{
	
}

bool Database::save()
{
	if (!m_FilePathSet) {
		LOGERR("File Path has not been set!");
		return false;
	}

	int n = 250;

	ProgressBar *bar = new ProgressBar(n, "Saving Database to File...");

	for (int i = 0; i <= n; ++i) {
		bar->Progressed(i);
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	std::cout << std::endl;

	TiXmlDocument dbDoc;
	TiXmlDeclaration * decl = new TiXmlDeclaration("1.0", "", "");
	dbDoc.LinkEndChild(decl);

	TiXmlElement * dbRoot = new TiXmlElement("database");
	dbDoc.LinkEndChild(dbRoot);

	TiXmlComment * comment = new TiXmlComment();
	comment->SetValue("Database Save from Borealis");
	dbRoot->LinkEndChild(comment);

	reorder();

	for (int i = 0; i < m_MemberCount; i++) {
		Member member = m_Members.at(i);

		TiXmlElement * memberRoot = new TiXmlElement("member");
		memberRoot->SetAttribute("ID", member.getID());

		TiXmlElement * memberName = new TiXmlElement("name");
		memberName->LinkEndChild(new TiXmlText((member.getName()).c_str()));
		memberRoot->LinkEndChild(memberName);

		TiXmlElement * memberSteamID = new TiXmlElement("steamid");
		memberSteamID->LinkEndChild(new TiXmlText((member.getSteamID()).c_str()));
		memberRoot->LinkEndChild(memberSteamID);

		TiXmlElement * memberAdminLevel = new TiXmlElement("adminlevel");
		memberAdminLevel->LinkEndChild(new TiXmlText((member.getAdminLevelByName().c_str())));
		memberRoot->LinkEndChild(memberAdminLevel);

		TiXmlElement * memberRank = new TiXmlElement("rank");
		memberRank->LinkEndChild(new TiXmlText((member.getRankByName(true).c_str())));
		memberRoot->LinkEndChild(memberRank);

		TiXmlElement * memberGroupName = new TiXmlElement("groupname");
		memberGroupName->LinkEndChild(new TiXmlText((member.getGroupNameByName().c_str())));
		memberRoot->LinkEndChild(memberGroupName);

		dbRoot->LinkEndChild(memberRoot);
	}

	return dbDoc.SaveFile(m_FilePath.c_str());
}

bool Database::load()
{
	if (!m_FilePathSet) {
		LOGERR("File Path has not been set!");
		return false;
	}
	TiXmlDocument dbDoc(m_FilePath.c_str());
	if (!dbDoc.LoadFile()) {
		LOGERR("Error loading file!");
		return false;
	}

	clear();
	
	int n = 100;

	ProgressBar *bar = new ProgressBar(n, "Loading Database from File...");

	for (int i = 0; i <= n; ++i) {
		bar->Progressed(i);
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	std::cout << std::endl;

	TiXmlHandle hDoc(&dbDoc);
	TiXmlElement * pElem;
	TiXmlElement * pChild;
	TiXmlHandle hRoot(0);
	
	pElem = hDoc.FirstChildElement().Element();

	if (!pElem) {
		LOGERR("Error loading database root element.");
		return false;
	}

	

	hRoot = TiXmlHandle(pElem);


	pElem = hRoot.FirstChild("member").Element();
	for (pElem; pElem; pElem = pElem->NextSiblingElement()) {


		int			memberID = 0;
		std::string memberName;
		std::string memberSteamID;
		Member::AdminLevels memberAdminLevel;
		Member::Ranks memberRank;
		Member::GroupNames memberGroupName;

		std::string adminLvlString;
		std::string rankString;
		std::string groupNameString;


		pElem->Attribute("ID", &memberID);

		pChild = pElem->FirstChildElement("name");	// Name
		memberName = pChild->GetText();

		//--------------------------------

		pChild = pElem->FirstChildElement("steamid");	// Steam ID
		memberSteamID = pChild->GetText();

		//--------------------------------

		pChild = pElem->FirstChildElement("adminlevel");	// Adminlevel
		adminLvlString = pChild->GetText();

		//--------------------------------

		pChild = pElem->FirstChildElement("rank");	// Rank
		rankString = pChild->GetText();

		//--------------------------------

		pChild = pElem->FirstChildElement("groupname");	// Group Name
		groupNameString = pChild->GetText();

		
		memberAdminLevel = Member::toAdminLevel(adminLvlString);
		memberRank = Member::toRank(rankString);
		memberGroupName = Member::toGroupName(groupNameString);

		addMember(new Member(
			memberID,
			memberName,
			memberSteamID,
			memberAdminLevel,
			memberRank
		));
		
	}

	

	return true;

}


int Database::addMember(Member& member)
{
	member.updateID(m_MemberCount);
	m_MemberCount++;
	m_Members.push_back(member);
	return member.getID();
}

int Database::addMember(Member * member)
{
	Member newMember (member);
	newMember.updateID(m_MemberCount);
	m_MemberCount++;
	m_Members.push_back(newMember);
	return newMember.getID();
}

bool Database::delMember(const int & id)
{
	return false;
}

void Database::update() const
{
}

void Database::clear()
{
	m_MemberCount = 0;
	m_Members.clear();
}

std::vector<Member> Database::getMembersByAdminLevel(Member::AdminLevels adminlvl)
{
	reorder();
	std::vector<Member> returnVector;

	for (int i = 0; i < m_MemberCount; i++) {

		if (m_Members.at(i).getAdminLevel() == adminlvl)
			returnVector.push_back(m_Members.at(i));
	}

	return returnVector;
}

void Database::setMemberAtIndex(const unsigned short & i, Member member)
{
	m_Members.at(i) = member;
	reorder();
}

void Database::deleteMemberAtIndex(const unsigned short & i)
{
	m_Members.erase(m_Members.begin() + i);
	m_MemberCount--;
	reorder();
}

void Database::reorder()
{
	std::vector<Member> newMemberList;
	unsigned short newMemberCount = 0;

	for (int h = 0; h < NUMBER_OF_ADMINLEVELS; h++) {
		for (int i = 0; i < NUMBER_OF_RANKS; i++) {
			for (int j = 0; j < m_MemberCount; j++) {
				Member member = m_Members.at(j);

				if (member.getAdminLevel() == (h + 1)) {
					if (member.getRank() == i) {

						member.updateID(newMemberCount);
						newMemberCount++;
						newMemberList.push_back(member);
					}
				}
			}
		}
	}

	clear();

	m_MemberCount = newMemberCount;
	m_Members = newMemberList;
}

std::ostream & operator<<(std::ostream & os, const Database & database)
{
	for (int i = 0; i < database.getMemberCount(); i++) {

		os << database.getMemberAtIndex(i);
	}

	return os;
}
