#pragma once

#include "../../pch.h"
#include "../database/database.h"

class BEC_Export
{
private:
	BEC_Export() { };
public:
	static bool exportData(std::string filepath, Database* database);
};

