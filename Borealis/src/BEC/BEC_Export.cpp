#include "BEC_Export.h"


bool BEC_Export::exportData(std::string filepath, Database* database)
{

	int n = 80;

	ProgressBar *bar = new ProgressBar(n, "Exporting Database for BEC...");

	for (int i = 0; i <= n; ++i) {
		bar->Progressed(i);
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	std::cout << std::endl;

	TiXmlDocument dbDoc;
	TiXmlDeclaration * decl = new TiXmlDeclaration("1.0", "UTF-8", "yes");
	dbDoc.LinkEndChild(decl);

	TiXmlElement * dbRoot = new TiXmlElement("BEAdmins");
	dbDoc.LinkEndChild(dbRoot);

	TiXmlComment * comment = new TiXmlComment();
	comment->SetValue("Database exported to BEC Admins XML format by Borealis");
	dbRoot->LinkEndChild(comment);

	database->reorder();

	for (int i = 0; i < database->getMemberCount(); i++) {
		Member member = database->getMemberAtIndex(i);

		TiXmlElement * memberRoot = new TiXmlElement("admin");
		memberRoot->SetAttribute("id", member.getID());

		TiXmlElement * memberName = new TiXmlElement("name");
		memberName->LinkEndChild(new TiXmlText((member.getName()).c_str()));
		memberRoot->LinkEndChild(memberName);

		TiXmlElement * memberGUID = new TiXmlElement("guid");
		memberGUID->LinkEndChild(new TiXmlText((member.getGUID().c_str())));
		memberRoot->LinkEndChild(memberGUID);

		TiXmlElement * memberAdminLevel = new TiXmlElement("group");
		memberAdminLevel->LinkEndChild(new TiXmlText((member.getAdminLevelByName().c_str())));
		memberRoot->LinkEndChild(memberAdminLevel);

		TiXmlElement * memberRank = new TiXmlElement("groupname");
		memberRank->LinkEndChild(new TiXmlText((member.getGroupNameByName().c_str())));
		memberRoot->LinkEndChild(memberRank);

		dbRoot->LinkEndChild(memberRoot);
	}

	return dbDoc.SaveFile(filepath.c_str());
}
