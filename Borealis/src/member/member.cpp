﻿#include "member.h"



Member::Member(unsigned short id, const std::string & name, std::string steamid, AdminLevels adminlvl, Ranks rank) :
	m_ID(id), m_Name(name), m_SteamID(steamid), m_AdminLevel(adminlvl), m_Rank(rank)
{

	try {
		if (!InputValidator::validate(m_SteamID, InputValidator::INPUTTYPE_STEAMID)) {
			throw std::string("SteamID");
		}
	}
	catch (std::string type) {
		std::cout << type << " is not valid for member " << m_Name << " (ID: " << m_ID << ")" << std::endl;
	}

	m_GUID = BEGUID::getBEGUID(m_SteamID);

	m_GroupName = makeGroupName();
}

Member::~Member()
{
}

Member::GroupNames Member::makeGroupName()
{

	if (m_Rank == RANK_CM) {

		return GROUP_COMMUNITY_MEMBER;
	}

	switch (m_AdminLevel) {
	case ADMIN_LEVEL_ONE:
		return GROUP_GLOBAL_ADMIN;
	case ADMIN_LEVEL_TWO:
		return GROUP_SENIOR_ADMIN;
	case ADMIN_LEVEL_THREE:
		return GROUP_SERVER_ADMIN;
	case ADMIN_LEVEL_FOUR:
		return GROUP_JUNIOR_ADMIN;
	case ADMIN_LEVEL_NULL:
	default:
		return GROUP_NULL;
	}
}

void Member::updateID(unsigned short newID)
{
	m_ID = newID;
}

void Member::updateName(const std::string & name)
{
	m_Name = name;
}

void Member::updateSteamID(const std::string & steamid)
{

	try {
		if (!InputValidator::validate(steamid, InputValidator::INPUTTYPE_STEAMID)) {
			throw std::string("SteamID");
		}
	}
	catch (std::string type) {
		std::cout << type << " is not valid for member " << m_Name << " (ID: " << m_ID << ")" << std::endl;
	}

	m_SteamID = steamid;
	m_GUID = BEGUID::getBEGUID(m_SteamID);
}

void Member::updateAdminLevel(AdminLevels adminlvl)
{
	m_AdminLevel = adminlvl;
	m_GroupName = makeGroupName();
}

void Member::updateRank(Ranks rank)
{
	m_Rank = rank;
	m_GroupName = makeGroupName();
}

void Member::updateGroupName(GroupNames groupname)
{
	m_GroupName = groupname;
}


std::string Member::getAdminLevelByName() const
{
	switch (m_AdminLevel) {
	case ADMIN_LEVEL_ONE:
		return "1";
	case ADMIN_LEVEL_TWO:
		return "2";
	case ADMIN_LEVEL_THREE:
		return "3";
	case ADMIN_LEVEL_FOUR:
		return "4";
	case ADMIN_LEVEL_NULL:
	default:
		return "0";
	}
}

std::string Member::getRankByName(const bool m) const
{
	if (m) {
		switch (m_Rank)
		{
		case RANK_CPT:
			return "Capt";
		case RANK_LT:
			return "Lt";
		case RANK_2LT:
			return "2Lt";
		case RANK_CM:
			return "CM";
		case RANK_WO:
			return "WO";
		case RANK_SGT:
			return "Sgt";
		case RANK_CPL:
			return "Cpl";
		case RANK_LCPL:
			return "LCpl";
		case RANK_PTE:
			return "Pte";
		case RANK_RCT:
			return "Rct";
		case RANK_UNRANKED:
		default:
			return "Unranked";
		}
	}
	else {
		switch (m_Rank)
		{
		case RANK_CPT:
			return "0";
		case RANK_LT:
			return "1";
		case RANK_2LT:
			return "2";
		case RANK_CM:
			return "3";
		case RANK_WO:
			return "4";
		case RANK_SGT:
			return "5";
		case RANK_CPL:
			return "6";
		case RANK_LCPL:
			return "7";
		case RANK_PTE:
			return "8";
		case RANK_RCT:
			return "9";
		case RANK_UNRANKED:
		default:
			return "10";
		}
	}
}

std::string Member::getGroupNameByName() const
{
	switch (m_GroupName) {
	case GROUP_GLOBAL_ADMIN:
		return "Global Admin";
	case GROUP_COMMUNITY_MEMBER:
		return "Community Member";
	case GROUP_SENIOR_ADMIN:
		return "Senior Admin";
	case GROUP_SERVER_ADMIN:
		return "Server Admin";
	case GROUP_JUNIOR_ADMIN:
		return "Junior Admin";
	case GROUP_NULL:
		return "";
	}
	return std::string();
}

Member::AdminLevels Member::toAdminLevel(const std::string lvlstring)
{
	if (lvlstring == "1")
		return ADMIN_LEVEL_ONE;
	else if (lvlstring == "2")
		return ADMIN_LEVEL_TWO;
	else if (lvlstring == "3")
		return ADMIN_LEVEL_THREE;
	else if (lvlstring == "4")
		return ADMIN_LEVEL_FOUR;
	else
		return ADMIN_LEVEL_NULL;
}

Member::Ranks Member::toRank(std::string rankstring)
{
	std::transform(rankstring.begin(), rankstring.end(), rankstring.begin(), ::toupper);

	if (rankstring == "CAPT")
		return RANK_CPT;
	else if (rankstring == "LT")
		return RANK_LT;
	else if (rankstring == "2LT")
		return RANK_2LT;
	else if (rankstring == "CM")
		return RANK_CM;
	else if (rankstring == "WO")
		return RANK_WO;
	else if (rankstring == "SGT")
		return RANK_SGT;
	else if (rankstring == "CPL")
		return RANK_CPL;
	else if (rankstring == "LCPL")
		return RANK_LCPL;
	else if (rankstring == "PTE")
		return RANK_PTE;
	else if (rankstring == "RCT")
		return RANK_RCT;
	else if (rankstring == "UNRANKED")
		return RANK_UNRANKED;
	else
		return RANK_NULL;
}

Member::GroupNames Member::toGroupName(std::string groupname)
{
	if (groupname == "Global Admin")
		return GROUP_GLOBAL_ADMIN;
	else if (groupname == "Community Member")
		return GROUP_COMMUNITY_MEMBER;
	else if (groupname == "Senior Admin")
		return GROUP_SENIOR_ADMIN;
	else if (groupname == "Server Admin")
		return GROUP_SERVER_ADMIN;
	else if (groupname == "Junior Admin")
		return GROUP_JUNIOR_ADMIN;
	else if (groupname == "")
		return GROUP_NULL;
	else
		return GROUP_NULL;
}

std::string Member::getRankByIndex(const short & index)
{
	switch (index) {

	case 0:
		return "Capt";
	case 1:
		return "Lt";
	case 2:
		return "2Lt";
	case 3:
		return "CM";
	case 4:
		return "WO";
	case 5:
		return "Sgt";
	case 6:
		return "Cpl";
	case 7:
		return "LCpl";
	case 8:
		return "Pte";
	case 9:
		return "Rct";
	case 10:
		return "Unranked";
	default:
		return "NULL";
	}
}

std::string Member::getGroupByIndex(const short & index)
{
	switch (index) {

	case 0:
		return "Global Admin";
	case 1:
		return "Community Member";
	case 2:
		return "Senior Admin";
	case 3:
		return "Server Admin";
	case 4:
		return "Junior Admin";
	default:
		return "NULL";
	}
}

std::string Member::getRankByEnum(Ranks e_num)
{
	switch (e_num) {

	case RANK_CPT:
		return "Capt";
	case RANK_LT:
		return "Lt";
	case RANK_2LT:
		return "2Lt";
	case RANK_CM:
		return "CM";
	case RANK_WO:
		return "WO";
	case RANK_SGT:
		return "Sgt";
	case RANK_CPL:
		return "Cpl";
	case RANK_LCPL:
		return "LCpl";
	case RANK_PTE:
		return "Pte";
	case RANK_RCT:
		return "Rct";
	case RANK_UNRANKED:
		return "Unranked";
	default:
		return "NULL";
	}
}

std::ostream & operator<<(std::ostream & os, const Member & member)
{
	os <<
		"	Member ID: " << member.getID() << std::endl <<
		"		Name: " << member.getName() << std::endl <<
		"		SteamID: " << member.getSteamID() << std::endl <<
		"		GUID: " << member.getGUID() << std::endl <<
		"		Adminlevel: " << member.getAdminLevelByName() << std::endl <<
		"		Rank: " << member.getRankByName() << std::endl <<
		"		Groupname: " << member.getGroupNameByName() << std::endl << std::endl;

	return os;
}
