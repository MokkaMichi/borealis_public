#pragma once

#include "../../pch.h"
#include "../InputValidator/InputValidator.h"
#include "../util/beguid.h"

#define NUMBER_OF_ADMINLEVELS 4
#define NUMBER_OF_RANKS 11
#define NUMBER_OF_GROUPS 5

class Member {

public:
	enum AdminLevels { 
		ADMIN_LEVEL_ONE = 1,
		ADMIN_LEVEL_TWO = 2,
		ADMIN_LEVEL_THREE = 3,
		ADMIN_LEVEL_FOUR = 4,
		ADMIN_LEVEL_NULL = -1
	};

	enum Ranks {
		RANK_CPT, 
		RANK_LT,
		RANK_2LT,
		RANK_CM,
		RANK_WO,
		RANK_SGT,
		RANK_CPL,
		RANK_LCPL,
		RANK_PTE,
		RANK_RCT,
		RANK_UNRANKED,
		RANK_NULL = -1
	};

	enum GroupNames {
		GROUP_GLOBAL_ADMIN,
		GROUP_COMMUNITY_MEMBER,
		GROUP_SENIOR_ADMIN,
		GROUP_SERVER_ADMIN,
		GROUP_JUNIOR_ADMIN,
		GROUP_NULL = -1
	};


private:
	unsigned short m_ID;
	std::string m_Name;
	std::string m_SteamID;
	std::string m_GUID;
	AdminLevels m_AdminLevel;
	Ranks m_Rank;
	GroupNames m_GroupName;

public:
	Member(unsigned short id, const std::string& name, std::string steamid, AdminLevels adminlvl, Ranks rank);

	Member(const Member& member) : m_ID(member.m_ID), m_Name(member.m_Name), m_SteamID(member.m_SteamID), m_GUID(member.m_GUID), m_AdminLevel(member.m_AdminLevel), m_Rank(member.m_Rank) {
		try {
			if (!InputValidator::validate(m_SteamID, InputValidator::INPUTTYPE_STEAMID)) {
				throw std::string("SteamID");
			}
		}
		catch (std::string type) {
			std::cout << type << " is not valid for member " << m_Name << " (ID: " << m_ID << ")" << std::endl;
		}

		m_GUID = BEGUID::getBEGUID(m_SteamID);

		m_GroupName = makeGroupName();
	}
	Member(Member* member) : m_ID(member->m_ID), m_Name(member->m_Name), m_SteamID(member->m_SteamID), m_GUID(member->m_GUID), m_AdminLevel(member->m_AdminLevel), m_Rank(member->m_Rank) {
		try {
			if (!InputValidator::validate(m_SteamID, InputValidator::INPUTTYPE_STEAMID)) {
				throw std::string("SteamID");
			}
		}
		catch (std::string type) {
			std::cout << type << " is not valid for member " << m_Name << " (ID: " << m_ID << ")" << std::endl;
		}

		m_GUID = BEGUID::getBEGUID(m_SteamID);

		m_GroupName = makeGroupName();
	}

	~Member();

	GroupNames makeGroupName();
	void updateID(unsigned short newID);
	void updateName(const std::string& name);
	void updateSteamID(const std::string& steamid);
	void updateAdminLevel(AdminLevels adminlvl);
	void updateRank(Ranks rank);
	void updateGroupName(GroupNames groupname);	// obsolete

	// Getter functions
	inline unsigned short getID() const { return m_ID; }
	inline std::string getName() const  { return m_Name; }
	inline std::string getSteamID() const { return m_SteamID; }
	inline std::string getGUID() const { return m_GUID; }
	inline AdminLevels getAdminLevel() const { return m_AdminLevel; }
	inline Ranks getRank(void) const { return m_Rank; }
	inline GroupNames getGroupName() const { return m_GroupName; }

	std::string getAdminLevelByName() const;
	std::string getRankByName(const bool m = true) const;
	std::string getGroupNameByName() const;


	static AdminLevels toAdminLevel(const std::string lvlstring);
	static Ranks toRank(std::string rankstring);
	static GroupNames toGroupName(std::string groupname);

	friend std::ostream& operator<< (std::ostream& os, const Member& member);

	static std::string getRankByIndex(const short& index);
	static std::string getGroupByIndex(const short& index);

	static std::string getRankByEnum(Ranks e_num);

};