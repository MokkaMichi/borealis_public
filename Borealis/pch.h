#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <memory>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <windows.h>
#include <iomanip>
#include <cstring>
#include <chrono>
#include <thread>
#include <conio.h>

#include "metadata.h"

#include "src/util/TinyXML/tinyxml.h"
#include "src/util/INIReader/INIReader.h"
#include "src/util/md5/md5.h"
#include "src/util/progressbars/progress_bar.h"
